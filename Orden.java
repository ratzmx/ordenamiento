/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ratzmx
 */
public class Orden {
    private int [] elementos = new int[10];
    public Orden(){
        this.elementos = new int [10];
        this.inicializar();
    }
    public Orden(int numel){
        this.elementos = new int[numel];
        this.inicializar();
    }
    private void inicializar(){
        for (int i = 0; i < this.elementos.length; i++) {
            elementos[i]=0;
        }
    }
    public void imprimirArreglo(){
        for (int i = 0; i < this.elementos.length; i++) {
            System.out.print("["+elementos[i]+"]");
        }
    }
    public void insertarElemento(int pos,int elemento){
        if (elemento<this.elementos.length && elemento>=0) {
            elementos[pos]=elemento;
        }
    }
    private int []copiarArreglo(){
        int [] nuevoArreglo= new int[this.elementos.length];
        for (int i = 0; i < this.elementos.length; i++) {
            nuevoArreglo[i]=elementos[i];
        }
        return nuevoArreglo;
    }
    public boolean buscar(int elemento){
        for (int x : elementos) {
            if (x==elemento){
                return true;
            }
        }
        return false;
    }
    private int tamArreglo(){
        return this.elementos.length;
    }
    private void imprimirArreglo(int[] arreglo){
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print("["+arreglo[i]+"]");
        }
    }
    public int [] burbuja(){
        int aux;
        int [] arreglo = this.copiarArreglo();
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = 1; j < arreglo.length; j++) {
                if (arreglo[j]<arreglo[j-1]) {
                    aux=arreglo[j];
                    arreglo[j]=arreglo[j-1];
                    arreglo[j-1]=aux;
                }
            }
        }
        this.imprimirArreglo(arreglo);
        return arreglo;
    }
    public int [] selection(){
        int [] arr = this.copiarArreglo();
        for (int i = 0; i < arr.length - 1; i++)
        {
            int indice = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[indice]) 
                    indice = j;
                    int numeroMenor = arr[indice];  
                    arr[indice] = arr[i];
                    arr[i] = numeroMenor;
        }
        System.out.println("");
        this.imprimirArreglo(arr);
        return arr;
    }
}